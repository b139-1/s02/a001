
Picking up where I left myself 8 years ago is my motivation for this course.

I have been working since 2013 in a small bar as an on-call DJ and MC while studying in college. In 2015, I travelled to Malaysia to continue my work as a DJ and MC in one of the most premiere club in Labuan F.T., Malaysia. For 5 years I have been working as a DJ, marketing manager and graphic designerm until the Covid 19 pandemic, wherein I lost my job and was forced to go home to the Philippines.